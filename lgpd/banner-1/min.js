window.addEventListener('load', () => {
  const key = `emiolo.lgpd.${window.location.host}`
  if (!localStorage || localStorage.getItem(key) === 'accept') return
  const banner = document.createElement('div')
  const text = document.createElement('div')
  const button = document.createElement('button')
  banner.append(text)
  banner.append(button)

  text.innerText = "Nós usamos cookies e outras tecnologias semelhantes para melhorar a sua experiência em nossos serviços, personalizar publicidade e recomendar conteúdo de seu interesse. Ao utilizar nossos serviços, você concorda com tal monitoramento."
  button.innerText = "ACEITO"

  banner.setAttribute('style', `
background: white;
position: fixed;
bottom: 0;
left: 0;
margin: 0;
display: flex;
width: 100vw;
padding: 16px;
padding-right: 24px;
align-items: center;
box-shadow: 0px 0px 2px 2px rgba(0,0,0,.25);
transform: translateY(100%);
transition: transform .3s;
z-index: 9999999999;
box-sizing: border-box;
  `)
  text.setAttribute('style', `
flex: 1;
font-family: sans-serif;
font-size: 12px;
padding-right: 8px;
margin: 0;
  `)
  button.setAttribute('style', `
background: #8BC34A;
padding: 8px 12px;
border-radius: 5px;
color: rgba(0,0,0,.82);
border: 0px;
font-family: sans-serif;
margin: 0;
  `)

  document.body.append(banner)
  setTimeout(() => {
    banner.style.transform = 'translateY(0%)'
  }, 1000)

  button.addEventListener('click', () => {
    banner.remove()
    localStorage.setItem(key, 'accept')
  })
  return banner
})
